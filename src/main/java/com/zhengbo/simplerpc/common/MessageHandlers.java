package com.zhengbo.simplerpc.common;

import com.sun.xml.internal.ws.api.handler.MessageHandler;
import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by zhengbo on 2019/8/20.
 */
public class MessageHandlers {

    private Map<String, IMessageHandler<?>> handlers = new ConcurrentHashMap<>();

    private IMessageHandler<MessageInput> defaultHandler;

    public void registry(String type, IMessageHandler<?> handler) {

        handlers.put(type, handler);
    }

    public MessageHandlers setDefaultHandler(IMessageHandler<MessageInput> defaultHandler) {
        this.defaultHandler = defaultHandler;
        return this;
    }

    public IMessageHandler<MessageInput> defaultHandler() {
        return defaultHandler;
    }

    public IMessageHandler<?> get(String type) {

        return handlers.get(type);
    }
}
