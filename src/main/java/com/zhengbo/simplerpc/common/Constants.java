package com.zhengbo.simplerpc.common;

import java.nio.charset.Charset;

/**
 * Created by zhengbo on 2019/8/20.
 */
public class Constants {

    public final static Charset DEFAULT_CHARSET = Charset.forName("utf8");
}
