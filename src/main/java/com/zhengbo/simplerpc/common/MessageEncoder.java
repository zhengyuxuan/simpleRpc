package com.zhengbo.simplerpc.common;

import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

/**
 * Created by zhengbo on 2019/8/21.
 */
public class MessageEncoder extends MessageToMessageEncoder<MessageOutput> {

    @Override
    protected void encode(ChannelHandlerContext ctx, MessageOutput msg, List<Object> out) throws Exception {

        ByteBuf buf = PooledByteBufAllocator.DEFAULT.directBuffer();
        writeStr(buf, msg.getRequestId());
        writeStr(buf, msg.getType());
        writeStr(buf, JSONObject.toJSONString(msg.getPlayLoad()));
        out.add(buf);
    }

    private void writeStr(ByteBuf buf, String s) {

        buf.writeInt(s.length());
        buf.writeBytes(s.getBytes(Constants.DEFAULT_CHARSET));
    }
}
