package com.zhengbo.simplerpc.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author zhengbo
 * @date 2019/8/20
 */
public class MessageRegistry {

    private final Map<String, Class<?>> classzs = new ConcurrentHashMap<>();

    public void registry(String type, Class<?> registryClass) {

        classzs.put(type, registryClass);
    }

    public Class<?> get(String type) {

        return classzs.get(type);
    }
}
