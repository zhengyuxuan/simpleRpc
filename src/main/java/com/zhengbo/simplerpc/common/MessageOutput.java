package com.zhengbo.simplerpc.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by zhengbo on 2019/8/20.
 */
@AllArgsConstructor
@Getter
public class MessageOutput {

    private String requestId;

    private String type;

    private Object playLoad;
}
