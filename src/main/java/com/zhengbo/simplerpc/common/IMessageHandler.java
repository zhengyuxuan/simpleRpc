package com.zhengbo.simplerpc.common;

import io.netty.channel.ChannelHandlerContext;

/**
 * Created by zhengbo on 2019/8/20.
 */
public interface IMessageHandler<T> {

    void handler(ChannelHandlerContext ctx, String requestId, T message);
}
