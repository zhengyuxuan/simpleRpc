package com.zhengbo.simplerpc.common;

import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;

/**
 * Created by zhengbo on 2019/8/20.
 */
@AllArgsConstructor
public class MessageInput {

    private String type;

    private String requestId;

    private String payLoad;

    public String getType() {
        return type;
    }

    public String getRequestId() {
        return requestId;
    }

    public <T> T getPayLoad(Class<T> classz) {

        if (payLoad == null) {
            return null;
        }

        return JSONObject.parseObject(payLoad, classz);
    }
}
