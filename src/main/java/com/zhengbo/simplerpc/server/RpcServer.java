package com.zhengbo.simplerpc.server;

import com.zhengbo.simplerpc.common.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by zhengbo on 2019/8/21.
 */
@Slf4j
public class RpcServer {

    private String ip;

    private int port;

    private int ioThreads;

    private int workThreads;

    private MessageHandlers handlers = new MessageHandlers();

    private MessageRegistry messageRegistry = new MessageRegistry();

    {
        this.handlers.setDefaultHandler(new DefaultHandler());
    }

    public RpcServer(String ip, int port, int ioThreads, int workThreads) {

        this.ip = ip;
        this.port = port;
        this.ioThreads = ioThreads;
        this.workThreads = workThreads;
    }

    private ServerBootstrap serverBootstrap;

    private EventLoopGroup eventLoopGroup;

    private ServerMessageCollector collector;

    private Channel serverChannel;

    public RpcServer addServer(String type, Class<?> requestClass, IMessageHandler<?> serverHandler) {

        this.messageRegistry.registry(type, requestClass);
        this.handlers.registry(type, serverHandler);
        return this;
    }

    public void startServer() {

        serverBootstrap = new ServerBootstrap();
        eventLoopGroup = new NioEventLoopGroup(ioThreads);
        serverBootstrap.group(eventLoopGroup);

        collector = new ServerMessageCollector(handlers, messageRegistry, workThreads);

        serverBootstrap.channel(NioServerSocketChannel.class).childHandler(
                new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) throws Exception {
                        ChannelPipeline pipeline = ch.pipeline();
                        pipeline.addLast(new ReadTimeoutHandler(60));
                        pipeline.addLast(new MessageDecoder());
                        pipeline.addLast(new MessageEncoder());
                        pipeline.addLast(collector);
                    }
                }
        );
        serverBootstrap.option(ChannelOption.SO_BACKLOG, 100).option(ChannelOption.SO_REUSEADDR, true)
                .option(ChannelOption.TCP_NODELAY, true).childOption(ChannelOption.SO_KEEPALIVE, true);

        serverChannel = serverBootstrap.bind(this.ip, this.port).channel();

        log.info("server started@ {}:{}", ip, port);
    }

    public void stop() {

        this.serverChannel.close();

        this.eventLoopGroup.shutdownGracefully();

        collector.closeGraceFully();
    }

}
