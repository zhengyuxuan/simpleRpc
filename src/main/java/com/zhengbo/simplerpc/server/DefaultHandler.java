package com.zhengbo.simplerpc.server;

import com.alibaba.fastjson.JSONObject;
import com.zhengbo.simplerpc.common.IMessageHandler;
import com.zhengbo.simplerpc.common.MessageInput;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zhengbo on 2019/8/21.
 */
public class DefaultHandler implements IMessageHandler<MessageInput> {

    private final static Logger logger = LoggerFactory.getLogger(DefaultHandler.class);

    @Override
    public void handler(ChannelHandlerContext ctx, String requestId, MessageInput message) {

        logger.error("unrecognized message:{}", JSONObject.toJSONString(message));
        ctx.close();
    }
}
