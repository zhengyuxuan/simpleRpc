package com.zhengbo.simplerpc.utils;

import java.util.UUID;

/**
 * Created by zhengbo on 2019/8/20.
 */
public class UUIDUtils {

    public final static String getUUIDString() {

        return UUID.randomUUID().toString();
    }
}
