package com.zhengbo.simplerpc.test;

import com.zhengbo.simplerpc.client.RpcClient;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by zhengbo on 2019/8/23.
 */
@Slf4j
public class DemoClient {

//    private RpcClient rpcClient;
//
//    public DemoClient(RpcClient rpcClient) {
//        this.rpcClient = rpcClient;
//    }


    public static void main(String[] args) throws InterruptedException {

        RpcClient rpcClient = new RpcClient("localhost", 9001);

        rpcClient.addRegistry("exp_res", ExpResponse.class);

        for (int i = 1; i < 30; i++) {
            //ExpResponse calcExp = (ExpResponse) rpcClient.send("calcExp", i);

            ExpResponse response = rpcClient.send("expCalc", new ExpRequest(2, i));
            Thread.sleep(100);
//            log.info("exp2({}) = {}, cost={}", i, response.getValue(), response.getCostInNanos());

            System.out.println("result:" + response.getValue() + ":" + response.getCostInNanos());
        }


        rpcClient.close();
    }
}
